<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('stories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('crime_id')->unsigned();
            $table->decimal('latitude', 11, 7)->default(0);
            $table->decimal('longitude', 11, 7)->default(0);
            $table->text('country')->nullable();
            $table->text('city')->nullable();
            $table->text('address')->nullable();
            $table->integer('date_id')->unsigned();
            $table->time('time')->nullable();
            $table->tinyInteger('did_police_showed_up')->nullable();
            $table->tinyInteger('did_crime_was_resolved')->nullable();
            $table->text('story', 300)->nullable()->charset('utf8mb4')->collate('utf8mb4_unicode_ci');
            $table->text('author', 30)->nullable();
            $table->tinyInteger('official')->default(0);
            $table->integer('reaction_1')->default(0);
            $table->integer('reaction_2')->default(0);
            $table->integer('reaction_3')->default(0);
            $table->integer('reaction_4')->default(0);
            $table->integer('reaction_5')->default(0);
            $table->integer('reaction_6')->default(0);
            $table->integer('reaction_7')->default(0);
            $table->integer('reaction_8')->default(0);
            $table->text('phone_id')->nullable();
            $table->text('user_ip')->nullable();
            $table->decimal('user_latitude', 11, 7)->default(0);
            $table->decimal('user_longitude', 11, 7)->default(0);
            $table->text('source')->nullable();
            $table->tinyInteger('flags')->default(0);
            $table->integer('related_to')->default(0);
            $table->integer('visits')->default(0);
            $table->foreign('crime_id')->references('id')->on('crimes');
            $table->foreign('date_id')->references('id')->on('dates');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('stories');
    }
}
