<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('dates', function (Blueprint $table) {
            $table->integer('id')->unsigned();
            $table->primary('id');
            $table->date('date');
            $table->string('day_of_month', 2);
            $table->string('day_name', 9);
            $table->char('day_of_week', 1);
            $table->string('day_of_week_in_month', 2);
            $table->string('day_of_week_in_year', 2);
            $table->string('day_of_quarter', 3);
            $table->string('day_of_year', 3);
            $table->string('week_of_month', 1);
            $table->string('week_of_quarter', 2);
            $table->string('week_of_year', 2);
            $table->string('month', 2);
            $table->string('month_name', 9);
            $table->string('month_of_quarter', 2);
            $table->char('quarter', 1);
            $table->string('quarter_name', 9);
            $table->char('year', 4);
            $table->char('year_name', 7);
            $table->char('month_year', 10);
            $table->char('mmyyyy', 6);
            $table->date('first_day_of_month');
            $table->date('last_day_of_month');
            $table->integer('days_in_month');
            $table->date('first_day_of_quarter');
            $table->date('last_day_of_quarter');
            $table->date('first_day_of_year');
            $table->date('last_day_of_year');
            $table->tinyInteger('is_weekday');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('dates');
    }
}
