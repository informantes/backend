<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->text('phone_id');
            $table->string('name', 30)->nullable();
            $table->string('email');
            $table->tinyInteger('sex')->nullable();
            $table->tinyInteger('age')->nullable();
            $table->tinyInteger('verified')->default(0);
            $table->tinyInteger('premium')->default(0);
            $table->integer('premium_since')->unsigned()->default(20000101);
            $table->string('password', 60)->nullable();
            $table->foreign('premium_since')->references('id')->on('dates');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
