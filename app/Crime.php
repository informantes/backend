<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crime extends Model
{
    protected $table = 'crimes';

    protected $fillable = [
		'name'
	];

	protected $hidden = ['created_at','updated_at'];
}
