<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    protected $table = 'stories';

    protected $fillable = [
		'*'
	];

	//protected $hidden = ['created_at','updated_at'];

	/**
     * NxN relationship with users table
     * Set relationship using foreign model name and database table
     *
     * @return relationship
     */
    public function dates()
    {
        return $this->belongsTo('Date');
    }

    public function crimes()
    {
        return $this->belongsTo('Crime');
    }
}
