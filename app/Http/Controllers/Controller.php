<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController
{
	use DispatchesJobs, ValidatesRequests;

	/**
     * 200 - OK
     * 400 - Bad Request
     * 500 - Internal Server Error
     */
     
    //HTTP status codes
     const STATUS_OK = 200;
     const STATUS_CREATED = 201;
     const STATUS_NO_CONTENT = 204;
     const STATUS_NOT_MODIFIED = 304;
     const STATUS_BAD_REQUEST = 400;
     const STATUS_UNAUTHORIZED = 401;
     const STATUS_FORBIDDEN = 403;
     const STATUS_NOT_FOUND = 404;
     const STATUS_CONFLICT = 409;
     const STATUS_UNPROCESSABLE_ENTITY = 422;
     const STATUS_INTERNAL_SERVER_ERROR = 500;
     const STATUS_NOT_IMPLEMENTED = 501;
     
    //PERSONALIZED CODES
     const STATUS_VALIDATION_ERROR = 100;    
     const STATUS_RECORD_NOT_FOUND = 101;
     const STATUS_RECORD_NOT_SAVED = 102;
     const STATUS_RECORD_NOT_MODIFIED = 103;
     const STATUS_RECORD_NOT_DELETED = 105;
     
     const STATUS_XMPP_ROOM_ERROR = 150;
     const STATUS_XMPP_USER_ERROR = 151;
     const STATUS_XMPP_JOIN_ERROR = 152;
     const STATUS_XMPP_LEAVE_ERROR = 153;
     
    //PERSONALIZED MESSAGES
     const MSG_RECORD_NOT_FOUND = 'Record not found.';
     const MSG_RECORD_NOT_SAVED = 'Record not saved.';
     const MSG_RECORD_CONFLICT = 'Record already exists.';
     const MSG_NOT_ENOUGH_COIN = 'Unable to purchase video, not enough coins.';
     const MSG_RECORD_NOT_MODIFIED = 'Record not modified.';
     const MSG_RECORD_NOT_DELETED = 'Record not deleted.';
     const MSG_XMPP_ROOM_ERROR = 'Error with XMPP room.';
     const MSG_XMPP_USER_ERROR = 'Error with XMPP user.';
     const MSG_XMPP_JOIN_ERROR = 'Error join to XMPP room.';
     const MSG_XMPP_LEAVE_ERROR = 'Error leave XMPP room.';
     const MSG_UNPROCESSABLE_ENTITY = 'Unprocessable entity.';

	protected $_response = array(
        'code' => self::STATUS_OK,
        //'data' => null, --> set only when is required
        //'error' => 'message', --> set only when is required
    );
}

