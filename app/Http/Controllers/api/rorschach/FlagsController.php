<?php

namespace App\Http\Controllers\api\rorschach;

use Validator;
use App\Http\Controllers\Controller;
use Response;
use Illuminate\Http\Request;

use Input;
use DB;
use stdClass;

// Model
use App\Flag;
use App\Story;

class FlagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        try{
            $rules = [
                'story_id' => 'required|integer',
                'phone_id' => 'required|string'
            ];

            $validator = Validator::make($request->all(), $rules);

            // Check validations
            if ($validator->fails()) {
                $this->_response = array(
                    'code' => self::STATUS_VALIDATION_ERROR,
                    'error' => $validator->errors()
                    );

                return Response::json($this->_response, self::STATUS_OK);
            } else {
                $story = Story::find(Input::get('story_id'));
                if(!$story){
                    $this->_response = array(
                        'code' => self::STATUS_RECORD_NOT_FOUND,
                        'error' => self::MSG_RECORD_NOT_FOUND
                        );

                    return Response::json($this->_response, self::STATUS_OK);
                }

                // Create new record in no_ways table
                $flag = new Flag;
                $flag->story_id = $request->input('story_id');
                $flag->phone_id = $request->input('phone_id');

                if(!$flag->save()){
                    $this->_response = array(
                        'code' => self::STATUS_RECORD_NOT_SAVED,
                        'error' => self::MSG_RECORD_NOT_SAVED
                        );

                    return Response::json($this->_response, self::STATUS_OK);
                };

                // Update Story table with the new flag value
                $story->flags = $story->flags + 1;

                if(!$story->save()){
                    $this->_response = array(
                        'code' => self::STATUS_RECORD_NOT_SAVED,
                        'error' => self::MSG_RECORD_NOT_SAVED
                        );

                    return Response::json($this->_response, self::STATUS_OK);
                };

                $this->_response = array(
                    'code' => self::STATUS_OK,
                    'data' => $story
                );
            }
        }
        catch (Exception $e) {
        $this->_response = array(
            'code' => self::STATUS_INTERNAL_SERVER_ERROR,
            'error' => true,
            'message' => $e->getMessage()
            );
        }

        //Response with the new record
        return Response::json($this->_response, self::STATUS_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
