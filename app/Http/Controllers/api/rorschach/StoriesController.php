<?php

namespace App\Http\Controllers\api\rorschach;

use Validator;
use App\Http\Controllers\Controller;
use Response;
use Illuminate\Http\Request;

use Input;
use DB;
use stdClass;
use DateTime;

// Model
use App\Crime;
use App\Date;
use App\Story;

// Services
use App\Services\GreatCircleDistanceService;
use App\Services\GeoIPService;

class StoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try{
            $timestamp = Input::get('timestamp');
            if($timestamp){
                $myDate = date('Y-m-d H:i:s', $timestamp);
                $stories = Story::where('created_at', '>=', $myDate)
                ->orderBy('created_at', 'asc')
                ->get(['id', 'longitude', 'latitude', 'crime_id', 'created_at'])
                ->toArray();
            }else{
                $stories = Story::orderBy('created_at', 'asc')
                ->get(['id', 'longitude', 'latitude', 'crime_id', 'created_at'])
                ->toArray();
            }
            
            if(!$stories){
                $this->_response = array(
                    'code' => self::STATUS_RECORD_NOT_FOUND,
                    'error' => self::MSG_RECORD_NOT_FOUND
                    );

                return Response::json($this->_response, self::STATUS_OK);
            }


            $this->_response = array(
                'code' => self::STATUS_OK,
                'data' => $stories
                );
        }catch (Exception $e) {
            $this->_response = array(
                'code' => self::STATUS_INTERNAL_SERVER_ERROR,
                'error' => true,
                'message' => $e->getMessage()
                );
        }

        //Response
        return Response::json($this->_response, self::STATUS_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getListedStoriesIp($ip, $type, $page ){
        try{
            $geoData = GeoIPService::getLocationInfo($ip);
            $myCountry = $geoData->country;
            $myCity = utf8_encode($geoData->city);

            $limit = 20;
            $page = $page + 1;
            $offset = ($limit * $page) - $limit;

            if($type == '0'){
                $stories = Story::where('city', '=', $myCity);
            }else{
                $stories = Story::where('country', '=', $myCountry);
            }
            
            $stories = $stories->where('flags', '=', 0)
                ->where('official', '=', 0)
                ->whereNotNull('story')
                ->orderBy('date_id', 'desc')
                ->orderBy('time', 'desc');

            $allStories = $stories->get();

            $stories = $stories->take($limit)
                ->skip($offset)
                ->get();

            if(!$stories){
                $this->_response = array(
                    'code' => self::STATUS_RECORD_NOT_FOUND,
                    'error' => self::MSG_RECORD_NOT_FOUND
                    );

                return Response::json($this->_response, self::STATUS_OK);
            }

            $storiesArray = [];
            $finalPage = 0;
            if(count($stories) < $limit || count($allStories) == $limit){
                $finalPage = 1;
            }

            foreach ($stories as $key => $value) {
                $storiesObj = new stdClass;
                $storiesObj->id = (int)$value->id;
                $storiesObj->crime_id = (int)$value->crime_id;
                $storiesObj->latitude = (float)$value->latitude;
                $storiesObj->city = $value->city;
                $storiesObj->address = $value->address;
                $storiesObj->longitude = (float)$value->longitude;
                $storiesObj->date = date('Y-m-d', strtotime($value->date_id)) . ' ' . $value->time;
                $storiesObj->story = $value->story;
                $storiesObj->author = $value->author;
                $storiesObj->reaction_1 = (int)$value->reaction_1;
                $storiesObj->reaction_2 = (int)$value->reaction_2;
                $storiesObj->reaction_3 = (int)$value->reaction_3;
                $storiesObj->reaction_4 = (int)$value->reaction_4;
                $storiesObj->reaction_5 = (int)$value->reaction_5;
                $storiesObj->reaction_6 = (int)$value->reaction_6;
                $storiesObj->reaction_7 = (int)$value->reaction_7;
                $storiesObj->reaction_8 = (int)$value->reaction_8;
                $storiesArray[] = $storiesObj;
            }

            $this->_response = array(
                'code' => self::STATUS_OK,
                'country' => $myCountry,
                'city' => $myCity,
                'final_page' => $finalPage,
                'data' => $storiesArray
            );
        }catch (Exception $e) {
            $this->_response = array(
                'code' => self::STATUS_INTERNAL_SERVER_ERROR,
                'error' => true,
                'message' => $e->getMessage()
                );
        }

        //Response
        return Response::json($this->_response, self::STATUS_OK);
    }

    static function getGeocodeNames($lat, $lng){
        $param = array('latlng' => $lat . ',' . $lng);
        $jsonResponse = \Geocoder::geocode('json', $param);
        $response = json_decode($jsonResponse, true);
        $geoCodeArray = [];
        $cityArray = [];
        $countryArray = [];

        if(count($response['results']) > 0){
            foreach ($response['results'] as $key => $value) {
                foreach ($value['address_components'] as $key2 => $value2){
                    if($value2['types'][0] == 'locality'){
                        $cityArray[] = $value2['long_name'];
                    }
                    if($value2['types'][0] == 'country'){
                        $countryArray[] = $value2['long_name'];
                    }
                }
            }

            if(count($cityArray) > 0){
                $geoCodeArray['city'] = $cityArray[0];
            }else{
                $geoCodeArray['city'] = '';
            }

            if(count($countryArray) > 0){
                $geoCodeArray['country'] = $countryArray[0];
            }else{
                $geoCodeArray['country'] = '';
            }

        }
        return $geoCodeArray;
    }

    static function validateCoordinates($lat, $lng){
        $param = array('latlng' => $lat . ',' . $lng);
        $jsonResponse = \Geocoder::geocode('json', $param);

        return $jsonResponse;
    }

    public function getGroupedStories($lat, $lng){
        try{
            $distance = 5;
            $current_year = date('Y');
            $last_year = (int)$current_year - 1 . date('md');
            $limit = 20;

            $nearest_points = DB::table('stories as c')
            ->select('c.id', 'crime_id', 'latitude', 'longitude', 'address', 'date_id', 'time', DB::raw('((ACOS(SIN('. $lat .' * PI() / 180) * SIN(latitude * PI() / 180) + COS('. $lat .' * PI() / 180) * COS(latitude * PI() / 180) * COS(('. $lng .' - longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515 * 1.609344) as distance'))
            ->where('flags', '=', 0)
            ->where('related_to', '=', 0)
            ->where('date_id', '>=', $last_year)
            ->having('distance', '<', $distance)
            ->groupBy('c.id')
            ->orderBy('distance')
            ->get();


            if(!$nearest_points){
                $this->_response = array(
                    'code' => self::STATUS_RECORD_NOT_FOUND,
                    'data' => []
                    );

                return Response::json($this->_response, self::STATUS_OK);
            }

            $nearest_points_array = [];
            $tmp = 0;
            $point = 0.02;

            $total_crimes = 1;

            $nearest_points_obj = new stdClass;
            $nearest_points_obj->id = (int)$nearest_points[0]->id;
            $nearest_points_obj->crime_id = (int)$nearest_points[0]->crime_id;
            $nearest_points_obj->latitude = (float)$nearest_points[0]->latitude;
            $nearest_points_obj->longitude = (float)$nearest_points[0]->longitude;
            $nearest_points_obj->address = $nearest_points[0]->address;
            $nearest_points_obj->date = date('Y-m-d', strtotime($nearest_points[0]->date_id)) . ' ' . $nearest_points[0]->time;
            //$nearest_points_obj->distance = (float)$nearest_points[0]->distance;
            $nearest_points_obj->total_crimes = $total_crimes++;
            $nearest_points_array[] = $nearest_points_obj;

            for ($i=1; $i < count($nearest_points); $i++) { 
                if($nearest_points[$i]->distance - $tmp > $point){
                    $total_crimes = 1;
                    $nearest_points_obj = new stdClass;
                    $nearest_points_obj->id = (int)$nearest_points[$i]->id;
                    $nearest_points_obj->crime_id = (int)$nearest_points[$i]->crime_id;
                    $nearest_points_obj->latitude = (float)$nearest_points[$i]->latitude;
                    $nearest_points_obj->longitude = (float)$nearest_points[$i]->longitude;
                    $nearest_points_obj->address = $nearest_points[0]->address;
                    $nearest_points_obj->date = date('Y-m-d', strtotime($nearest_points[$i]->date_id)) . ' ' . $nearest_points[$i]->time;
                    //$nearest_points_obj->distance = (float)$nearest_points[$i]->distance;
                    $nearest_points_obj->total_crimes = $total_crimes++;
                    $nearest_points_array[] = $nearest_points_obj;
                    $tmp = $nearest_points[$i]->distance;
                }else{
                    $nearest_points_obj->total_crimes = $total_crimes++;
                }
            }

            $this->_response = array(
                'code' => self::STATUS_OK,
                'data' => $nearest_points_array
            );

        }catch (Exception $e) {
            $this->_response = array(
                'code' => self::STATUS_INTERNAL_SERVER_ERROR,
                'error' => true,
                'message' => $e->getMessage()
                );
        }

        //Response
        return Response::json($this->_response, self::STATUS_OK);
    }

    public function getListedStoriesUserLocation($lat, $lng, $type, $page, $name){
        try{
            $distance = 5;
            $current_year = date('Y');
            $last_year = (int)$current_year - 1 . date('md');

            $myCountry = '';
            $myCity = '';

            if($name == 'null'){
                $locationInfo = $this->getGeocodeNames($lat, $lng);
                $myCountry = $locationInfo['country'];
                $myCity = $locationInfo['city'];
            }else if($type == 0){
                $myCity = $name;
            }else{
                $myCountry = $name;
            }
            
            $limit = 20;
            $page = $page + 1;
            $offset = ($limit * $page) - $limit;

            $nearest_points_array = [];

            $nearest_points = DB::table('stories as c')
            ->select('c.id', 'crime_id', 'latitude', 'longitude', 'address', 'date_id', 'time', 'story', 'author', 'official', 'reaction_1', 'reaction_2', 'reaction_3', 'reaction_4', 'reaction_5', 'reaction_6', 'reaction_7', 'reaction_8', 'country', 'city')
            ->where('flags', '=', 0)
            ->where('related_to', '=', 0);
            if($type == 0){
                $nearest_points = $nearest_points->where('city', '=', $myCity);
            }else{
                $nearest_points = $nearest_points->where('country', '=', $myCountry);
            }
            $nearest_points = $nearest_points->where('date_id', '>=', $last_year)
            ->groupBy('c.id')
            ->orderBy('date_id', 'desc')
            ->orderBy('time', 'desc');

            $allStories = $nearest_points->get();

            $nearest_points = $nearest_points->take($limit)
            ->skip($offset)
            ->get();

            if(!$nearest_points){
                $this->_response = array(
                    'code' => self::STATUS_RECORD_NOT_FOUND,
                    'country' => $myCountry,
                    'city' => $myCity,
                    'final_page' => 0,
                    'data' => []
                    );

                return Response::json($this->_response, self::STATUS_OK);
            }

            $finalPage = 0;
            if(count($nearest_points) < $limit || count($allStories) == $limit){
                $finalPage = 1;
            }

            foreach ($nearest_points as $key => $value) {
                $nearest_points_obj = new stdClass;
                $nearest_points_obj->id = (int)$nearest_points[$key]->id;
                $nearest_points_obj->crime_id = (int)$nearest_points[$key]->crime_id;
                $nearest_points_obj->latitude = (float)$nearest_points[$key]->latitude;
                $nearest_points_obj->longitude = (float)$nearest_points[$key]->longitude;
                $nearest_points_obj->address = $nearest_points[$key]->address;
                $nearest_points_obj->date = date('Y-m-d', strtotime($nearest_points[$key]->date_id)) . ' ' . $nearest_points[$key]->time;
                $nearest_points_obj->story = $nearest_points[$key]->story;
                $nearest_points_obj->author = $nearest_points[$key]->author;
                $nearest_points_obj->official = (int)$nearest_points[$key]->official;
                $nearest_points_obj->reaction_1 = (int)$nearest_points[$key]->reaction_1;
                $nearest_points_obj->reaction_2 = (int)$nearest_points[$key]->reaction_2;
                $nearest_points_obj->reaction_3 = (int)$nearest_points[$key]->reaction_3;
                $nearest_points_obj->reaction_4 = (int)$nearest_points[$key]->reaction_4;
                $nearest_points_obj->reaction_5 = (int)$nearest_points[$key]->reaction_5;
                $nearest_points_obj->reaction_6 = (int)$nearest_points[$key]->reaction_6;
                $nearest_points_obj->reaction_7 = (int)$nearest_points[$key]->reaction_7;
                $nearest_points_obj->reaction_8 = (int)$nearest_points[$key]->reaction_8;
                //$nearest_points_obj->distance = (float)$nearest_points[$key]->distance;
                $nearest_points_array[] = $nearest_points_obj;
            }
        
            $this->_response = array(
                'code' => self::STATUS_OK,
                'country' => $myCountry,
                'city' => $myCity,
                'final_page' => $finalPage,
                'data' => $nearest_points_array
            );

        }catch (Exception $e) {
            $this->_response = array(
                'code' => self::STATUS_INTERNAL_SERVER_ERROR,
                'error' => true,
                'message' => $e->getMessage()
                );
        }

        //Response
        return Response::json($this->_response, self::STATUS_OK);
    }

    public function getFilteredStories($lat, $lng, $day, $time){
        try{
            $distance = 0.1;
            $timeRange1 = ['', ''];
            $timeRange2 = ['', ''];

            if($time == 1){
                $timeRange1 = ['20:00:00', '23:59:59'];
                $timeRange2 = ['00:00:00', '05:59:59'];
            }else if($time == 2){
                $timeRange1 = ['06:00:00', '11:59:59'];
            }else if($time == 3){
                $timeRange1 = ['12:00:00', '16:59:59'];
            }else if($time == 4){
                $timeRange1 = ['17:00:00', '19:59:59'];
            }

            $nearest_points = DB::table('stories as c')
            ->select('c.id', 'crime_id', 'latitude', 'longitude', 'address', 'date_id', 'time', 'story', 'author', 'official', 'reaction_1', 'reaction_2', 'reaction_3', 'reaction_4', 'reaction_5', 'reaction_6', 'reaction_7', 'reaction_8', DB::raw('((ACOS(SIN('. $lat .' * PI() / 180) * SIN(latitude * PI() / 180) + COS('. $lat .' * PI() / 180) * COS(latitude * PI() / 180) * COS(('. $lng .' - longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515 * 1.609344) as distance'))
            ->join('dates as d', 'c.date_id', '=', 'd.id')
            ->where('flags', '=', 0)
            ->where('related_to', '=', 0)
            ->where('day_of_week', '=', $day)
            ->where(function($query2) use ($timeRange1, $timeRange2){
                $query2->whereBetween('time', $timeRange1)
                       ->orWhere(function($query3) use ($timeRange2){
                            $query3->whereBetween('time', $timeRange2);
                        });
            });
            $nearest_points = $nearest_points->groupBy('c.id')
            ->having('distance', '<', $distance)
            ->orderBy('date_id', 'desc')
            ->orderBy('time', 'desc')
            ->get();

            if(!$nearest_points){
                $this->_response = array(
                    'code' => self::STATUS_RECORD_NOT_FOUND,
                    'total_crimes' => 0,
                    'most_common' => 0,
                    'since' => date('Y'),
                    'data' => []
                    );

                return Response::json($this->_response, self::STATUS_OK);
            }

            $nearest_points_array = [];
            $crimes_array = [];

            foreach ($nearest_points as $key => $value) {
                // All cases
                $nearest_points_obj = new stdClass;
                $nearest_points_obj->id = (int)$value->id;
                $nearest_points_obj->crime_id = (int)$value->crime_id;
                $nearest_points_obj->latitude = (float)$value->latitude;
                $nearest_points_obj->longitude = (float)$value->longitude;
                $nearest_points_obj->address = $value->address;
                $nearest_points_obj->date = date('Y-m-d', strtotime($value->date_id)) . ' ' . $value->time;
                $nearest_points_obj->story = $value->story;
                $nearest_points_obj->author = $value->author;
                $nearest_points_obj->official = (int)$value->official;
                $nearest_points_obj->reaction_1 = (int)$value->reaction_1;
                $nearest_points_obj->reaction_2 = (int)$value->reaction_2;
                $nearest_points_obj->reaction_3 = (int)$value->reaction_3;
                $nearest_points_obj->reaction_4 = (int)$value->reaction_4;
                $nearest_points_obj->reaction_5 = (int)$value->reaction_5;
                $nearest_points_obj->reaction_6 = (int)$value->reaction_6;
                $nearest_points_obj->reaction_7 = (int)$value->reaction_7;
                $nearest_points_obj->reaction_8 = (int)$value->reaction_8;
                $nearest_points_array[] = $nearest_points_obj;
                $crimes_array[] = (int)$value->crime_id;
            }

            // Get the most common crime
            $crimes_array = array_count_values($crimes_array); 
            arsort($crimes_array);
            $most_common_case = array_keys($crimes_array)[0];
            $n_most_common_case = $crimes_array[$most_common_case];
            $total_crimes = count($nearest_points_array);

            $this->_response = array(
                'code' => self::STATUS_OK,
                'total_crimes' => $total_crimes,
                'most_common' => $most_common_case,
                'since' => date('Y', strtotime($nearest_points[$total_crimes - 1]->date_id)),
                'data' => $nearest_points_array
            );

        }catch (Exception $e) {
            $this->_response = array(
                'code' => self::STATUS_INTERNAL_SERVER_ERROR,
                'error' => true,
                'message' => $e->getMessage()
                );
        }

        //Response
        return Response::json($this->_response, self::STATUS_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        try{
            $rules = [
                'crime_id' => 'required|integer',
                'latitude' => 'required|regex:/^\-?\d*(\.\d+)?$/',
                'longitude' => 'required|regex:/^\-?\d*(\.\d+)?$/',
                'country' => 'string',
                'city' => 'string',
                'address' => 'string',
                'date_id' => 'required|integer',
                'time' => 'required|date_format:H:i',
                'did_police_showed_up' => 'integer',
                'did_crime_was_resolved' => 'integer',
                'story' => 'string',
                'author' => 'string',
                'phone_id' => 'string',
                'user_ip' => 'string|regex:/^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/',
                'user_latitude' => 'regex:/^\-?\d*(\.\d+)?$/',
                'user_longitude' => 'regex:/^\-?\d*(\.\d+)?$/',
                'source' => 'required'
            ];

            $validator = Validator::make($request->all(), $rules);

            // Check validations
            if ($validator->fails()) {
                $this->_response = array(
                    'code' => self::STATUS_VALIDATION_ERROR,
                    'error' => $validator->errors()
                    );

                return Response::json($this->_response, self::STATUS_OK);
            } else {
                $locationInfo = $this->getGeocodeNames($request->input('latitude'), $request->input('longitude'));
                $story = new Story();
                $story->crime_id = $request->input('crime_id');
                $story->latitude = $request->input('latitude');
                $story->longitude = $request->input('longitude');
                $story->country = $locationInfo['country'];
                $story->city = $locationInfo['city'];
                $story->address = $request->input('address');
                $story->date_id = $request->input('date_id');
                $story->time = $request->input('time');
                $story->did_police_showed_up = $request->input('did_police_showed_up');
                $story->did_crime_was_resolved = $request->input('did_crime_was_resolved');
                $story->story = $request->input('story');
                $story->author = $request->input('author');
                $story->phone_id = $request->input('phone_id');
                $story->user_ip = $request->input('user_ip');
                $story->user_latitude = $request->input('user_latitude');
                $story->user_longitude = $request->input('user_longitude');
                $story->source = $request->input('source');

                $myCase = $story::where(['date_id' => $story->date_id, 'crime_id' => $story->crime_id])->get();
                if(!empty($myCase[0])){
                    $distance = GreatCircleDistanceService::getDistance($myCase[0]->longitude, $myCase[0]->latitude, $story->longitude, $story->latitude);
                    // Validate if the distance are less 500 meters and time less 30 minutes for relate the case with other
                    if((int)$distance <= 500 && abs(strtotime($myCase[0]->time) - strtotime($story->time)) <= 1800){
                        $story->related_to = $myCase[0]->id;
                    }
                }

                if(!$story->save()){
                    $this->_response = array(
                        'code' => self::STATUS_RECORD_NOT_SAVED,
                        'error' => self::MSG_RECORD_NOT_SAVED
                        );

                    return Response::json($this->_response, self::STATUS_OK);
                }
                
                $my_story = new stdClass;
                $my_story->id = (int)$story->id;
                $my_story->crime_id = (int)$story->crime_id;
                $my_story->latitude = (float)$story->latitude;
                $my_story->longitude = (float)$story->longitude;
                $my_story->address = $story->address;
                $my_story->date = date('Y-m-d', strtotime($story->date_id)) . ' ' . $story->time . ':00';
                $my_story->author = $story->author;
                $my_story->story = $story->story;

                $this->_response = array(
                    'code' => self::STATUS_OK,
                    'data' => [$my_story]
                    );
            }
        }catch (Exception $e) {
            $this->_response = array(
                'code' => self::STATUS_INTERNAL_SERVER_ERROR,
                'error' => true,
                'message' => $e->getMessage()
                );
        }

        //Response with the new record
        return Response::json($this->_response, self::STATUS_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $current_point = Story::find($id);

            if(!$current_point){
                $this->_response = array(
                    'code' => self::STATUS_RECORD_NOT_FOUND,
                    'error' => self::MSG_RECORD_NOT_FOUND
                    );

                return Response::json($this->_response, self::STATUS_OK);
            }

            $my_story = new stdClass;
            $my_story->id = $current_point->id;
            $my_story->crime_id = $current_point->crime_id;
            $my_story->latitude = $current_point->latitude;
            $my_story->longitude = $current_point->longitude;
            $my_story->address = $current_point->address;
            $my_story->date = date('Y-m-d', strtotime($current_point->date_id)) . ' ' . $current_point->time;
            $my_story->story = $current_point->story;
            $my_story->author = $current_point->author;
            $my_story->official = $current_point->official;
            $my_story->reaction_1 = $current_point->reaction_1;
            $my_story->reaction_2 = $current_point->reaction_2;
            $my_story->reaction_3 = $current_point->reaction_3;
            $my_story->reaction_4 = $current_point->reaction_4;
            $my_story->reaction_5 = $current_point->reaction_5;
            $my_story->reaction_6 = $current_point->reaction_6;
            $my_story->reaction_7 = $current_point->reaction_7;
            $my_story->reaction_8 = $current_point->reaction_8;
            
            $current_point->visits = $current_point->visits + 1;

            if(!$current_point->save()){
                $this->_response = array(
                    'code' => self::STATUS_RECORD_NOT_SAVED,
                    'error' => self::MSG_RECORD_NOT_SAVED
                    );

                return Response::json($this->_response, self::STATUS_OK);
            }

            $current_year = date('Y');
            $last_year = (int)$current_year - 1 . date('md');

            $similar = Story::where('crime_id', '=', $current_point->crime_id)
                       ->where('city', '=', $current_point->city)
                       ->where('date_id', '>=', $last_year)
                       ->where('related_to', '=', 0)
                       ->count();


            $this->_response = array(
                'code' => self::STATUS_OK,
                'country' => $current_point->country,
                'city' => $current_point->city,
                'visits' => $current_point->visits,
                'similar' => $similar,
                'data' => $my_story
            );

        }catch (Exception $e) {
            $this->_response = array(
                'code' => self::STATUS_INTERNAL_SERVER_ERROR,
                'error' => true,
                'message' => $e->getMessage()
                );
        }

        //Response with the new record
        return Response::json($this->_response, self::STATUS_OK);
    }

    public function setReactions(Request $request){
        try{
            $rules = [
                'story_id' => 'required|integer',
                'reaction' => 'required|integer|max:8'
            ];

            $validator = Validator::make($request->all(), $rules);

            // Check validations
            if ($validator->fails()) {
                $this->_response = array(
                    'code' => self::STATUS_VALIDATION_ERROR,
                    'error' => $validator->errors()
                    );

                return Response::json($this->_response, self::STATUS_OK);
            } else {
                $story = Story::find($request->input('story_id'));
                $nReactions = $story->{'reaction_' . $request->input('reaction')};
                $story->{'reaction_' . $request->input('reaction')} = $nReactions + 1;

                if(!$story->save()){
                    $this->_response = array(
                        'code' => self::STATUS_RECORD_NOT_SAVED,
                        'error' => self::MSG_RECORD_NOT_SAVED
                        );

                    return Response::json($this->_response, self::STATUS_OK);
                }

                $this->_response = array(
                    'code' => self::STATUS_OK,
                    'data' => $story
                    );
            }
        }catch (Exception $e) {
            $this->_response = array(
                'code' => self::STATUS_INTERNAL_SERVER_ERROR,
                'error' => true,
                'message' => $e->getMessage()
                );
        }

        //Response with the new record
        return Response::json($this->_response, self::STATUS_CREATED);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
