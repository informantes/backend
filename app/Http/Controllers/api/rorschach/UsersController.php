<?php

namespace App\Http\Controllers\api\rorschach;

use Validator;
use App\Http\Controllers\Controller;
use Response;
use Illuminate\Http\Request;

use Input;
use DB;
use Crypt;

// Model
use App\User;
use App\Date;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $rules = [
                'email' => 'required|email',
                'phone_id' => 'required|string'
            ];

            $validator = Validator::make($request->all(), $rules);

            // Check validations
            if ($validator->fails()) {
                $this->_response = array(
                    'code' => self::STATUS_VALIDATION_ERROR,
                    'error' => $validator->errors()
                );

                return Response::json($this->_response, self::STATUS_OK);
            } else {
                //$myPassword = Crypt::encrypt($request->input('email'));
                $user = User::where('phone_id', '=', $request->input('phone_id'))->first();
                if(!$user){
                    $user = new User;
                }
                
                $user->phone_id = $request->input('phone_id');
                $user->name = $request->input('name');
                $user->email = $request->input('email');
                $user->sex = $request->input('sex');
                $user->age = $request->input('age');

                if(!$user->save()){
                    $this->_response = array(
                        'code' => self::STATUS_RECORD_NOT_SAVED,
                        'error' => self::MSG_RECORD_NOT_SAVED
                    );

                    return Response::json($this->_response, self::STATUS_OK);
                }


                $this->_response = array(
                    'code' => self::STATUS_OK,
                    'data' => $user
                );
            }
        }catch (Exception $e) {
            $this->_response = array(
                'code' => self::STATUS_INTERNAL_SERVER_ERROR,
                'error' => true,
                'message' => $e->getMessage()
            );
        }

        //Response with the new record
        return Response::json($this->_response, self::STATUS_CREATED);

    }

    public function setUserPremium(Request $request){
        try{
            $rules = [
                'phone_id' => 'required|string'
            ];

            $validator = Validator::make($request->all(), $rules);

            // Check validations
            if ($validator->fails()) {
                $this->_response = array(
                    'code' => self::STATUS_VALIDATION_ERROR,
                    'error' => $validator->errors()
                );

                return Response::json($this->_response, self::STATUS_OK);
            } else {
                $user = User::where('phone_id', '=', $request->input('phone_id'))->first();

                if(count($user) == 0){
                    $this->_response = array(
                        'code' => self::STATUS_RECORD_NOT_FOUND,
                        'error' => self::MSG_RECORD_NOT_FOUND
                    );

                    return Response::json($this->_response, self::STATUS_OK);
                }

                $user->premium = 1;
                $user->premium_since = date('Ymd');
                if(!$user->save()){
                    $this->_response = array(
                        'code' => self::STATUS_RECORD_NOT_SAVED,
                        'error' => self::MSG_RECORD_NOT_SAVED
                    );

                    return Response::json($this->_response, self::STATUS_OK);
                }


                $this->_response = array(
                    'code' => self::STATUS_OK,
                    'data' => $user
                );
            }
        }catch (Exception $e) {
            $this->_response = array(
                'code' => self::STATUS_INTERNAL_SERVER_ERROR,
                'error' => true,
                'message' => $e->getMessage()
            );
        }

        //Response with the new record
        return Response::json($this->_response, self::STATUS_OK);
    }

    public function deleteUser(Request $request){
        try{
            $rules = [
                'phone_id' => 'required|string'
            ];

            $validator = Validator::make($request->all(), $rules);

            // Check validations
            if ($validator->fails()) {
                $this->_response = array(
                    'code' => self::STATUS_VALIDATION_ERROR,
                    'error' => $validator->errors()
                );

                return Response::json($this->_response, self::STATUS_OK);
            } else {
                $user = User::where('phone_id', '=', $request->input('phone_id'))->first();

                if(count($user) == 0){
                    $this->_response = array(
                        'code' => self::STATUS_RECORD_NOT_FOUND,
                        'error' => self::MSG_RECORD_NOT_FOUND
                    );

                    return Response::json($this->_response, self::STATUS_OK);
                }

                if(!$user->delete()){
                    $this->_response = array(
                        'code' => self::STATUS_RECORD_NOT_SAVED,
                        'error' => self::MSG_RECORD_NOT_SAVED
                    );

                    return Response::json($this->_response, self::STATUS_OK);
                }


                $this->_response = array(
                    'code' => self::STATUS_NO_CONTENT,
                    'message' => 'User deleted.' 
                );
            }
        }catch (Exception $e) {
            $this->_response = array(
                'code' => self::STATUS_INTERNAL_SERVER_ERROR,
                'error' => true,
                'message' => $e->getMessage()
            );
        }

        //Response with the new record
        return Response::json($this->_response, self::STATUS_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
