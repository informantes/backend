<?php

namespace App\Http\Controllers\api\rorschach;

use Validator;
use App\Http\Controllers\Controller;
use Response;
use Illuminate\Http\Request;

use Input;

// Model
use App\Crime;

class CrimesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //Response with the new record
        return "Hello World";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        try{
            $rules = [
                'name' => 'required|alpha'
            ];

            $validator = Validator::make($request->all(), $rules);

            // Check validations
            if ($validator->fails()) {
                $this->_response = array(
                    'code' => self::STATUS_VALIDATION_ERROR,
                    'error' => $validator->errors()
                );

                return Response::json($this->_response, self::STATUS_OK);
            } else {
                $crime = new Crime();
                $crime->name = $request->input('name');

                if(!$crime->save()){
                    $this->_response = array(
                        'code' => self::STATUS_RECORD_NOT_SAVED,
                        'error' => self::MSG_RECORD_NOT_SAVED
                    );

                    return Response::json($this->_response, self::STATUS_OK);
                }


                $this->_response = array(
                    'code' => self::STATUS_OK,
                    'data' => $crime
                );
            }
        }catch (Exception $e) {
            $this->_response = array(
                'code' => self::STATUS_INTERNAL_SERVER_ERROR,
                'error' => true,
                'message' => $e->getMessage()
            );
        }

        //Response with the new record
        return Response::json($this->_response, self::STATUS_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
