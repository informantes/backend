<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
  return view('welcome');
});

/*
  |---------------------------------------------------------------------------
  | URL PATTERNS
  |---------------------------------------------------------------------------
 */
  Route::pattern('id', '[0-9]+');
  Route::pattern('type', '[0-9]+');
  Route::pattern('city', '[0-9]+');
  Route::pattern('zoom', '[0-9]+');
  Route::pattern('page', '[0-9]+');
  Route::pattern('longitude', '^[+-]?\d+\.\d+');
  Route::pattern('latitude', '^[+-]?\d+\.\d+');
  Route::pattern('ip', '^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$');

/*
  |---------------------------------------------------------------------------
  | API Rorschach version
  |---------------------------------------------------------------------------
 */
  Route::group(['prefix' => 'rorschach'], function() {

  // Validate coordinates
    Route::get('validate/{latitude}/{longitude}', 'api\rorschach\StoriesController@validateCoordinates');

	// Crimes
    Route::resource('crimes', 'api\rorschach\CrimesController');

	// Stories
    Route::resource('stories', 'api\rorschach\StoriesController');
    Route::get('stories/{latitude}/{longitude}', 'api\rorschach\StoriesController@getGroupedStories');
    Route::get('stories/{ip}/{type}/{page}', 'api\rorschach\StoriesController@getListedStoriesIp');
    Route::get('stories/{latitude}/{longitude}/{type}/{page}/{name}', 'api\rorschach\StoriesController@getListedStoriesUserLocation');
    Route::get('stories/{latitude}/{longitude}/{day}/{time}', 'api\rorschach\StoriesController@getFilteredStories');

  // Reactions
    Route::post('reactions', 'api\rorschach\StoriesController@setReactions');

  // Flags
    Route::resource('flags', 'api\rorschach\FlagsController');

  // Users
    Route::resource('users', 'api\rorschach\UsersController');
    Route::put('users', 'api\rorschach\UsersController@setUserPremium');
    Route::delete('users', 'api\rorschach\UsersController@deleteUser');
  });