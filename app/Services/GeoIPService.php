<?php

namespace App\Services;

use stdClass;

class GeoIPService {
    /**
     * Calculates the great-circle distance between two points, with
     * the Vincenty formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    public static function getLocationInfo($ip)
    {
        // convert from degrees to radians
        $gi = geoip_open(public_path() . '/GeoIP/GeoIP.dat', GEOIP_STANDARD);
        $country = geoip_country_name_by_addr($gi, $ip);
        geoip_close($gi);

        $gic = geoip_open(public_path() . '/GeoIP/GeoLiteCity.dat', GEOIP_STANDARD);
        $city = geoip_record_by_addr($gic, $ip);
        geoip_close($gic);

        $geoData = new stdClass;
        $geoData->country = $country;
        $geoData->city = $city->city;

        return $geoData;
    }
}